#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <opencv2/objdetect/objdetect.hpp>

#include <QKeyEvent>
#include <QTime>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

/**
 * @author Yoann MASSET
 * @brief La classe MainWindow :
 * Classe qui gère la fenetre principale
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * @brief Constructeur de la classe MainWindow
     * @param parent : le widget parent
     */
    MainWindow(QWidget *parent = nullptr);
    /**
     * @brief Destructeur de la classe MainWindow
     */
    ~MainWindow();
    /**
     * @brief Méthode appelée lors de l'appui d'une touche sur le clavier
     * @param event : l'evenement relié à l'appui de la touche
     */
    void keyPressEvent(QKeyEvent * event);
    /**
     * @brief Méthode pour déterminer le déplacement du vaisseau en focntion des mains
     */
    void check_hand();

private slots:
    /// Le slots executé lorsque le chronomètre se met à jour
    void updateChronometre();
    /// Le slots executé lorsque la caméra se met à jour
    void update_camera();
    /// Le slots executé lorsqu'on clique sur le menu "pause"
    void Pause();
    /// Le slots executé lorsqu'on clique sur le menu "camera"
    void camera();
    /// Le slots executé lorsqu'on clique sur le menu "facile"
    void Easy();
    /// Le slots executé lorsqu'on clique sur le menu "normal"
    void Normal();
    /// Le slots executé lorsqu'on clique sur le menu "difficile"
    void Hard();
    /// Le slots executé lorsqu'on clique sur le menu "nouvelle partie"
    void newGame() { chronometre_ = QTime(0,0); emit NewGame(); };
    /// Le slots executé lorsqu'on clique sur le menu "aide"
    void Help();
    /// Le slots executé lorsqu'on clique sur le menu "credits"
    void Credits();
    /// Le slots executé lorsque la partie est gagnée
    void gameWin();
    /// Le slots executé lorsque la partie est perdue
    void gameLose();

signals:
    /// Le signal emit lorsque le vaisseau tourne à droite
    void goRight();
    /// Le signal emit lorsque le vaisseau tourne à gauche
    void goLeft();
    /// Le signal emit lorsque le vaisseau avance
    void goForward();
    /// Le signal emit lorsque le vaisseau monte
    void goUp();
    /// Le signal emit lorsque le vaisseau descend
    void goDown();
    /// Le signal emit lorsqu'on est en pause
    void widgetPause();
    /// Le signal emit lorsqu'on met la difficulté en facile
    void setEasy();
    /// Le signal emit lorsqu'on met la difficulté en normal
    void setNormal();
    /// Le signal emit lorsqu'on met la difficulté en difficile
    void setHard();
    /// Le signal emit lorsqu'on recommence une nouvelle partie
    void NewGame();

private:
    /// Le timer pour mettre à jour le chronomètre
    QTimer *timerChronometre_;
    /// Le chronomètre de la partie
    QTime chronometre_ = QTime(0,0);
    /// Le design de la fenetre
    Ui::MainWindow *ui;
    /// La capture de la WebCam
    cv::VideoCapture * webCam_;
    /// La taille de l'image capturée
    cv::Size size_;
    /// Le détecteur de mains fermées
    cv::CascadeClassifier closed_frontal_palm;
    /// Le détecteur de mains ouvertes
    cv::CascadeClassifier palm;
    /// Les rectangles qui définissent les mains fermées
    std::vector<cv::Rect> rect_closed_frontal_palm;
    /// Les rectangles qui définissent les mains ouvertes
    std::vector<cv::Rect> rect_palm;
    /// /// L'Attribut de pause
    bool pause = 0;
};
#endif // MAINWINDOW_H

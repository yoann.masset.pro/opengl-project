#include "background.h"

Background::Background()
{
    // Création de la quadrique
    background_ = gluNewQuadric();
}

Background::~Background()
{
    // Destruction de la quadrique
    if(background_)
        gluDeleteQuadric(background_);
}

void Background::display(GLuint * textures) const
{
    // Rendu de la sphère plus "lumineux"
    GLfloat color[] = {1.f, 1.f, 1.f, 1.f};
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, color);

    // Chargement de la texture de fond
    glBindTexture(GL_TEXTURE_2D, textures[0]);
    gluQuadricTexture(background_, GLU_TRUE);

    glPushMatrix();
    glRotatef(90.f, 1.f, 0.f, 0.f);
    // Creation d'un sphère quasi infinie pour le jeu
    gluSphere(background_, 1000, 100, 100);
    glPopMatrix();
}

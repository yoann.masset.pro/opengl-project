#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QTimer>
#include <QDebug>
#include <QMessageBox>

bool CAMERA = 1; // Activation ou non de la camera

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    // Mise en place de l'interface
    ui->setupUi(this);
    ui->pause->setVisible(0);
    ui->actionEasy->setChecked(1);
    ui->actionCamera->setChecked(CAMERA);

    // Mise en place de la camera
    if(CAMERA)
    {
        QTimer *timer_camera = new QTimer(this);
        connect(timer_camera, SIGNAL(timeout()), this, SLOT(update_camera()));
        timer_camera->start(100); //100ms

        webCam_=new cv::VideoCapture(0);
        if(!webCam_->isOpened())
        {
            ui->label_->setText("Error openning the default camera !");
        }
        closed_frontal_palm.load("../ProjetBDM/XML/closed_frontal_palm.xml");
        palm.load("../ProjetBDM/XML/open_frontal_palm.xml");
    }

    // Mise en place du chronomètre
    timerChronometre_ = new QTimer(this);
    connect(timerChronometre_, SIGNAL(timeout()), this, SLOT(updateChronometre()));
    timerChronometre_->start(1000);

    // Connexion des signaux pour les mouvemements du vaisseau
    connect(this, SIGNAL(goRight()), ui->widget_, SLOT(goRight()));
    connect(this, SIGNAL(goLeft()), ui->widget_, SLOT(goLeft()));
    connect(this, SIGNAL(goForward()), ui->widget_, SLOT(goForward()));
    connect(this, SIGNAL(goUp()), ui->widget_, SLOT(goUp()));
    connect(this, SIGNAL(goDown()), ui->widget_, SLOT(goDown()));

    // Connexion des signaux de la barre de menu
    connect(ui->actionNew_game, &QAction::triggered, this, &MainWindow::newGame);
    connect(ui->actionPlay_Pause, &QAction::triggered, this, &MainWindow::Pause);
    connect(ui->actionCamera, &QAction::triggered, this, &MainWindow::camera);
    connect(ui->actionEasy, &QAction::triggered, this, &MainWindow::Easy);
    connect(ui->actionNormal, &QAction::triggered, this, &MainWindow::Normal);
    connect(ui->actionHard, &QAction::triggered, this, &MainWindow::Hard);
    connect(ui->actionHelp, &QAction::triggered, this, &MainWindow::Help);
    connect(ui->actionCredits, &QAction::triggered, this, &MainWindow::Credits);

    // Connexion des siganux du jeu vers le widget OpenGL
    connect(this, SIGNAL(widgetPause()), ui->widget_, SLOT(widgetPause()));
    connect(this, SIGNAL(NewGame()), ui->widget_, SLOT(newGame()));
    connect(this, SIGNAL(setEasy()), ui->widget_, SLOT(setEasy()));
    connect(this, SIGNAL(setNormal()), ui->widget_, SLOT(setNormal()));
    connect(this, SIGNAL(setHard()), ui->widget_, SLOT(setHard()));

    // Connexion des signaux de fin de partie vers l'application principale
    connect(ui->widget_, SIGNAL(win()), this, SLOT(gameWin()));
    connect(ui->widget_, SIGNAL(lose()), this, SLOT(gameLose()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::updateChronometre(){
    // Mise à jour du chronomètre toutes les secondes
    chronometre_ = chronometre_.addSecs(1);
    ui->chrono->setText(chronometre_.toString("hh:mm:ss"));
}

void MainWindow::update_camera()
{
    if(CAMERA && !pause)
    {
        if (webCam_->isOpened()) {
            cv::Mat image;
            // Capture de la camera
            if (webCam_->read(image)) {   // Capture a frame

                size_ = image.size();

                // Inverser l'image comme un miroir
                flip(image,image,1);

                // Inverse la moitié droite de l'image pour une meilleure détection des poings
                cv::Rect rect_half_right = cv::Rect(size_.width/2,0,size_.width/2,size_.height);
                cv::Mat half_right = image(rect_half_right);
                flip(half_right, half_right, 1);
                image(rect_half_right) = half_right;

                // Inversion du cannal bleu et rouge
                cvtColor(image,image,cv::COLOR_BGR2RGB);

                // Conversion en niveau de gris
                cv::Mat img_gray;
                cvtColor(image,img_gray,cv::COLOR_BGR2GRAY);
                equalizeHist(img_gray, img_gray);

                // Detection des mains
                closed_frontal_palm.detectMultiScale(img_gray, rect_closed_frontal_palm, 1.1, 4, 0|cv::CASCADE_SCALE_IMAGE, cv::Size(60, 60));
                palm.detectMultiScale(img_gray, rect_palm, 1.1, 4, 0|cv::CASCADE_SCALE_IMAGE, cv::Size(60, 60));

                // Affichages des rectanges autour des mains détectées
                for (int i=0;i<(int)rect_closed_frontal_palm.size();i++)
                    rectangle(image,rect_closed_frontal_palm[i],cv::Scalar(255,0,0),2);
                for (int i=0;i<(int)rect_palm.size();i++)
                    rectangle(image,rect_palm[i],cv::Scalar(0,0,255),2);

                // Inversion une nouvelle fois de la moitié droite pour retrouver l'image originale
                half_right = image(rect_half_right);
                flip(half_right, half_right, 1);
                image(rect_half_right) = half_right;

                // Conversion de la matrice en une image
                QImage img = QImage((const unsigned char*)(image.data),image.cols,image.rows,QImage::Format_RGB888);
                img = img.scaled(300, 300,Qt::KeepAspectRatio);
                // Affiche l'image dans le label
                ui->label_->setPixmap(QPixmap::fromImage(img));
                ui->label_->resize(ui->label_->pixmap().size());

                // Vérification de la position des mains
                check_hand();
            }
            else {
                ui->label_->setText("Error capturing the frame");
            }
        }
    }
}

void MainWindow::keyPressEvent(QKeyEvent * event)
{
    // Evenement d'appui sur les touches du clavier
    if(!pause){
        switch (event->key()) {
        case Qt::Key_Right: case Qt::Key_D:
            emit goRight();
            break;
        case Qt::Key_Left: case Qt::Key_Q:
            emit goLeft();
            break;
        case Qt::Key_Up: case Qt::Key_Z:
            emit goUp();
            break;
        case Qt::Key_Down: case Qt::Key_S:
            emit goDown();
            break;
        case Qt::Key_Space:
            emit goForward();
            break;
        }
    }
}

void MainWindow::check_hand()
{
    // Vérification pour les poings
    if(rect_closed_frontal_palm.size() == 2)
    {
        // Détection du poings droit et gauche
        cv::Rect left_hand, right_hand;
        if(rect_closed_frontal_palm[0].x < rect_closed_frontal_palm[1].x){
            left_hand = rect_closed_frontal_palm[0];
            right_hand = rect_closed_frontal_palm[1];
        } else {
            left_hand = rect_closed_frontal_palm[1];
            right_hand = rect_closed_frontal_palm[0];
        }

        if(left_hand.y + left_hand.height < right_hand.y){ // Poing gauche au dessus du droit : mouvement à gauche
            emit goRight();
        }else if (right_hand.y + right_hand.height < left_hand.y){ // Poing droit au dessus du gauche : mouvement à droite
            emit goLeft();
        }else if(left_hand.y < size_.height*0.25 && right_hand.y < size_.height*0.25){ // Les 2 poings dans le quart superieur : mouvement vers le haut
            emit goUp();
        }else if (left_hand.y + left_hand.height > size_.height*0.75 && right_hand.y + right_hand.height > size_.height*0.75){ // Les 2 poings dans le quart inferieur : mouvement vers le bas
            emit goDown();
        }
    } else if(rect_palm.size() == 2) { // Vérification pour les mains ouvertes
        if( (rect_palm[0].x+rect_palm[0].width < rect_palm[1].x) || (rect_palm[1].x+rect_palm[1].width < rect_palm[0].x) ) {
            emit goForward();
        }
    }
}

void MainWindow::Pause(){
    // Mise en place de la pause
    pause = !pause;
    if(pause) {
        ui->pause->setVisible(1);
        timerChronometre_->stop();
    } else {
        ui->pause->setVisible(0);
        timerChronometre_->start();
    }
    emit widgetPause();
}

void MainWindow::camera(){
    CAMERA = !CAMERA;
    ui->label_->setText("CAMERA DISABLED!");
}

void MainWindow::Easy(){
    // Changement de la dificulté en facile
    if(ui->actionEasy->isChecked()) { // Si on est pas déja en facile
        ui->actionNormal->setChecked(0);
        ui->actionHard->setChecked(0);
        emit setEasy();
    } else {
        ui->actionEasy->setChecked(1);
    }
}

void MainWindow::Normal(){
    // Changement de la dificulté en normal
    if(ui->actionNormal->isChecked()){ // Si on est pas déja en normal
        ui->actionEasy->setChecked(0);
        ui->actionHard->setChecked(0);
        emit setNormal();
    } else {
        ui->actionNormal->setChecked(1);
    }
}

void MainWindow::Hard(){
    // Changement de la dificulté en difficile
    if(ui->actionHard->isChecked()){ // Si on est pas déja en difficile
        ui->actionEasy->setChecked(0);
        ui->actionNormal->setChecked(0);
        emit setHard();
    } else {
        ui->actionHard->setChecked(1);
    }
}

void MainWindow::gameWin(){
    Pause();
    QString text = "You WIN!\nYour time : " + chronometre_.toString();
    QMessageBox msgBox;
    msgBox.setText(text);
    msgBox.exec();
    Pause();
    newGame();
}

void MainWindow::gameLose(){
    Pause();
    QString text = "You LOSE!\nYour time : " + chronometre_.toString();
    QMessageBox msgBox;
    msgBox.setText(text);
    msgBox.exec();
    Pause();
    newGame();
}

void MainWindow::Help(){
    Pause();
    QString text = "Help\nThe aim of the game is to reach the space station by avoiding all the asteroids"
                   "\nTo move, you must either use the keyboard keys or use your hands in front of the camera: "
                   "open hands to move forward and closed fists to navigate (one higher than the other to turn and both up or down to tilt)";
    QMessageBox msgBox;
    msgBox.setText(text);
    msgBox.exec();
    Pause();
}

void MainWindow::Credits(){
    Pause();
    QString text = "Credits\nDesigned by Diane PERES and Yoann MASSET\nTSE - May 2022";
    QMessageBox msgBox;
    msgBox.setText(text);
    msgBox.exec();
    Pause();
}

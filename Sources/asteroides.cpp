#include "asteroides.h"

#include <QDebug>

Asteroides::Asteroides(int nbrAsteroides)
{
    // Création de la quadrique
    asteroides_ = gluNewQuadric();
    nbrAsteroides_ = nbrAsteroides;
}

Asteroides::~Asteroides(){
    // Destruction de la quadrique
    if(asteroides_)
        gluDeleteQuadric(asteroides_);
}

void Asteroides::Init(int nb){
    nbrAsteroides_ = nb;
    // Initialisation du tabelau de position
    pos = new QVector4D[nbrAsteroides_];
    QVector4D temp;
    for (int b = 0; b < nbrAsteroides_; b++)  {
        do{
            temp.setW(float(rand() % 100 + 1) / 10.0); // Rayon entre 0.1 et 10
            // [-50 ; -11] U [11 ; 50]
            temp.setX((rand() % 40 + 11) * ((rand() % 2 - 0.5) / 0.5));
            temp.setY((rand() % 40 + 11) * ((rand() % 2 - 0.5) / 0.5));
            temp.setZ((rand() % 40 + 11) * ((rand() % 2 - 0.5) / 0.5));
        } while (GetCollision(temp) == 1); // Tant qu'il y a collisions
        pos[b] = temp;
    }
}

void Asteroides::Display(GLuint * textures)
{
    // Chargement de la texture de l'asteroide
    glBindTexture(GL_TEXTURE_2D, textures[1]);
    gluQuadricTexture(asteroides_, GLU_TRUE);

    for (int b = 0; b < nbrAsteroides_; b++)  {
        glPushMatrix ();
        // Affichage de l'asteroide
        glTranslatef (pos[b].x(), pos[b].y(), pos[b].z());
        gluSphere(asteroides_,pos[b].w(), 10, 10);
        glPopMatrix();
    }
}

int Asteroides::GetCollision(QVector4D object){
    for (int b = 0; b < nbrAsteroides_;b++){
        QVector3D pos_asteroide = pos[b].toVector3D();
        QVector3D pos_object = object.toVector3D();
        // Distance entre le centre de l'asteroide et le centre de l'objet
        float distance = pos_asteroide.distanceToPoint(pos_object);
        if (distance < (pos[b].w() + object.w())){
            return 1; // COLLISION
        }
    }
    return 0; // PAS DE COLLISION
}

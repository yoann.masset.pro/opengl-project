#include "vaisseau.h"

Vaisseau::Vaisseau()
{
    // Création de la quadrique
    vaisseau_ = gluNewQuadric();
}

Vaisseau::~Vaisseau()
{
    // Destruction de la quadrique
    if(vaisseau_)
        gluDeleteQuadric(vaisseau_);
}

void Vaisseau::display(GLuint * textures) const
{
    float e = 0.05f; //epaisseaur coté

    // Chargement de la texture de coté
    glBindTexture(GL_TEXTURE_2D, textures[2]);

    // Dessin des 4 exagones qui définissent les cotés
    for (int i = 0; i < 4; i++) {
        float j = i*e + (l - 3*e)*((int) i/2) - l/2;

        glBegin(GL_POLYGON);

        glNormal3f(0.f, 0.f, 1.f);
        glTexCoord2f(0.f,.5f); glVertex3f(j,0.f,-.5f);
        glTexCoord2f(.18f,1.f); glVertex3f(j,.5f,-.3f);
        glTexCoord2f(.82f,1.f); glVertex3f(j,.5f,.3f);
        glTexCoord2f(1.f,.5f); glVertex3f(j,0.f,.5f);
        glTexCoord2f(.82f,0.f); glVertex3f(j,-.5f,.3f);
        glTexCoord2f(.18f,0.f); glVertex3f(j,-.5f,-.3f);

        glEnd();
    }

    //Les 6 faces pour finaliser chaque coté
    for (int i = 0; i < 2; i++) {
        float j = (l-e)*i - l/2;

        glBegin(GL_QUADS);

        //Les 3 faces du haut

        glVertex3f(j,0.f,-.5f);
        glVertex3f(j,.5f,-.3f);
        glVertex3f(j+e,.5f,-.3f);
        glVertex3f(j+e,0.f,-.5f);

        glVertex3f(j,.5f,-.3f);
        glVertex3f(j,.5f,.3f);
        glVertex3f(j+e,.5f,.3f);
        glVertex3f(j+e,.5f,-.3f);

        glVertex3f(j,0.f,.5f);
        glVertex3f(j,.5f,.3f);
        glVertex3f(j+e,.5f,.3f);
        glVertex3f(j+e,0.f,.5f);

        //Les 3 faces du bas

        glVertex3f(j,0.f,-.5f);
        glVertex3f(j,-.5f,-.3f);
        glVertex3f(j+e,-.5f,-.3f);
        glVertex3f(j+e,0.f,-.5f);

        glVertex3f(j,-.5f,-.3f);
        glVertex3f(j,-.5f,.3f);
        glVertex3f(j+e,-.5f,.3f);
        glVertex3f(j+e,-.5f,-.3f);

        glVertex3f(j,0.f,.5f);
        glVertex3f(j,-.5f,.3f);
        glVertex3f(j+e,-.5f,.3f);
        glVertex3f(j+e,0.f,.5f);

        glEnd();
    }

    // Chargement de la texture pour le cockpit
    glBindTexture(GL_TEXTURE_2D, textures[3]);
    gluQuadricTexture(vaisseau_, GLU_TRUE);

    glPushMatrix();
    glRotatef(-100.f, 1.f, 0.f, 0.f);
    gluSphere(vaisseau_, e*3, 30, 30);
    glPopMatrix();

    // Chargelent de la texture pour relié le coté et le cockpit
    glBindTexture(GL_TEXTURE_2D, textures[4]);

    glPushMatrix();
    glTranslatef(-l/2, 0.f, 0.f);
    glRotatef(90.f, 0.f, 1.f, 0.f);
    glRotatef(-130.f, 0.f, 0.f, 1.f);
    gluCylinder(vaisseau_, e, e, l, 30, 30);
    glPopMatrix();
}

#include "station.h"

Station::Station() {
    // Création de la quadrique
    station_ = gluNewQuadric();
    pos = QVector4D();
}

Station::~Station(){
    // Destruction de la quadrique
    if(station_)
        gluDeleteQuadric(station_);
}

void Station::Init(Asteroides &asteroides_){
    do{
        // [-50 ; -11] U [11 ; 50]
        pos.setX((rand() % 40 + 11) * ((rand() % 2 - 0.5) / 0.5));
        pos.setY((rand() % 40 + 11) * ((rand() % 2 - 0.5) / 0.5));
        pos.setZ((rand() % 40 + 11) * ((rand() % 2 - 0.5) / 0.5));
        pos.setW(3.f); // RADIUS
    } while (asteroides_.GetCollision(pos)==1); // Tant qu'il y a collision entre la station et un asteroide
}

void Station::Display(GLuint * textures, float rotation){

    // Chrome
    GLfloat chrome_ambiente[] = {.25, .25, .25, 1.0};
    glMaterialfv(GL_FRONT, GL_AMBIENT, chrome_ambiente);
    GLfloat chrome_diffuse[] = {.4, .4, .4, 1.0};
    glMaterialfv(GL_FRONT, GL_DIFFUSE, chrome_diffuse);
    GLfloat chrome_speculaire[] = {.774597, .774597, .774597, 1.0};
    glMaterialfv(GL_FRONT, GL_SPECULAR, chrome_speculaire);
    float chrome_shininess = 76.8f;
    glMaterialf(GL_FRONT, GL_SHININESS, chrome_shininess);
    GLfloat chrome_emission[] = {0.0, 0.0, 0.0, 1.0};
    glMaterialfv(GL_FRONT, GL_EMISSION, chrome_emission);

    // Lumière de la station
    GLfloat light_tab[] = {pos.x(), pos.y(), pos.z()-3, 1.0}; //positionnelle
    glLightfv(GL_LIGHT1, GL_POSITION, light_tab);
    GLfloat light_ambiante_tab[] = {0, 1.0, 0, 1.0};
    glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambiante_tab);
    GLfloat light_dir[] = {0.0, -1.0, 0.0};
    glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, light_dir);
    glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, 90);

    glTranslatef(pos.x(), pos.y(), pos.z());
    glRotatef(-90, 1, 0, 0);
    glRotatef(rotation, 0, 0, 1); // Animation de la station

    // Chargement de texture
    glBindTexture(GL_TEXTURE_2D, textures[6]);
    gluQuadricTexture(station_, GLU_TRUE);

    // Dessin de la planete centrale
    glPushMatrix();
    gluSphere(station_,2.5, 20, 20);
    glPopMatrix();
    gluQuadricTexture(station_, GLU_FALSE);

    // Dessin des anneaux
    glPushMatrix();
    glTranslatef(-0.25, 0, 0);
    glRotatef(90, 0, 1, 0);
    gluCylinder(station_,3, 3, 0.5, 20, 20);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(0, 0.25, 0);
    glRotatef(90, 1, 0, 0);
    gluCylinder(station_,3, 3, 0.5, 20, 20);
    glPopMatrix();

    // Dessin du cylindre
    glPushMatrix ();
    glTranslatef(0, 0, -3);
    gluCylinder(station_,0.1,0.1,7,20,20);
    glPopMatrix();

    // Lumière de la station
    if(((int)rotation/10) % 2 == 0){
        GLfloat light_emission[] = {0.0, 1.0, 0.0, 1.0};
        glMaterialfv(GL_FRONT, GL_EMISSION, light_emission);
        glEnable(GL_LIGHT1);
    }
    else{
        GLfloat light_emission[] = {0.0, 0.0, 0.0, 1.0};
        glMaterialfv(GL_FRONT, GL_EMISSION, light_emission);
        glDisable(GL_LIGHT1);
    }

    glPushMatrix ();
    glTranslatef(0, 0, -3.1);
    gluSphere(station_,0.15, 20, 20);
    glPopMatrix();

    // Dessin du drapeau
    glBindTexture(GL_TEXTURE_2D, textures[5]);
    glPushMatrix();
    glTranslatef(0, 0, 3);
    glRotated(90,1,0,0);

    glBegin(GL_QUADS);

    glTexCoord2d(1,1); glVertex3f(0,0,0);
    glTexCoord2d(0,1); glVertex3f(1,0,0);
    glTexCoord2d(0,0); glVertex3f(1,1,0);
    glTexCoord2d(1,0); glVertex3f(0,1,0);

    glEnd();
    glPopMatrix();
}

int Station::GetCollision(QVector4D object){
    float distance = object.toVector3D().distanceToPoint(pos.toVector3D());
    if (distance < (pos.w() + object.w())){
        return 1;
    } else {
        return 0;
    }
}

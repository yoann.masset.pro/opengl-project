#ifndef STATION_H
#define STATION_H

#include <GL/glu.h>
#include "asteroides.h"
#include <QVector4D>

/**
 * @author Diane PERES
 * @brief La classe Station :
 * La classe qui gère la station spaciale
 */
class Station
{
public:
    /**
     * @brief Constructeur de la classe Station
     */
    Station();
    /**
     * @brief Destructeur de la classe Station
     */
    ~Station();
    /**
     * @brief Méthode permettant d'initialiser la station. Permet de la placer aléatoirement dans la scène
     * @param asteroides_ : tous les astéroides de la scène (pour pas qu'il y ait de collision entre la sattion et les asteroides)
     */
    void Init(Asteroides &asteroides_);
    /**
     * @brief Méthode pour dessiner la station dans la scène 3D
     * @param textures : pointeur vers le tableau de toutes les textures
     * @param rotation : l'angle de rotation de la station
     */
    void Display(GLuint * textures, float rotation = 0);
    /**
     * @brief Méthode pour détecter la collision entre la station et un autre objet
     * @param object : l'objet à tester
     * @return 1 s'il y a collison entre la station et l'objet, 0 sinon
     */
    int GetCollision(QVector4D object);

private:
    /// La quadrique pour dessiner la station
    GLUquadric* station_;
    /// Le vecteur contenant la position de la station (x, y, z) et le rayon de la station (w)
    QVector4D pos;
};

#endif // STATION_H

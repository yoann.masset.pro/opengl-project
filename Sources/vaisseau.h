#ifndef VAISSEAU_H
#define VAISSEAU_H

#include <GL/glu.h>
#include <QVector3D>

/**
 * @author Diane PERES
 * @brief La classe Vaisseau :
 * La classe qui gère le vaisseau
 */
class Vaisseau
{
public:
    /**
     * @brief Constructeur de la classe Vaisseau
     */
    Vaisseau();
    /**
     * @brief Destructeur de la classe Vaisseau
     */
    ~Vaisseau();
    /**
     * @brief Méthode pour dessiner le vaisseau dans la scène 3D
     * @param textures : pointeur vers le tableau de toutes les textures
     */
    void display(GLuint * textures) const;
    /**
     * @brief Recupère la position du vaisseau
     * @return la position du vaisseau
     */
    QVector3D getPos() const { return pos_; };
    /**
     * @brief Modifie la position du vaisseau
     * @param La nouvelle position du vaisseau
     */
    void setPos(QVector3D pos) { pos_ = pos; };
    /**
     * @brief Récupère la largeur du vaisseau
     * @return La largeur du vaisseau
     */
    float getLargeur() const { return l; };

private:
    /// Le vecteur contenant la position du vaisseau (x, y, z)
    QVector3D pos_ = QVector3D(0.f, 0.f, 0.f);
    /// La quadrique pour dessiner le vaisseau
    GLUquadric * vaisseau_ {nullptr};
    /// La largeur du vaisseau
    float l = .8f;
};

#endif // VAISSEAU_H

#ifndef ASTEROIDES_H
#define ASTEROIDES_H

#include <qopengl.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <GL/glu.h>
#include <QVector4D>

/**
 * @author Diane PERES
 * @brief La classe Asteroides :
 * Cette classe permet de gérer les astéroides de la scène
 */
class Asteroides
{
public:
    /**
     * @brief Constructeur de la classe Asteroides
     * @param nbrAsteroides : le nombre d'astéroides dans la scène
     */
    Asteroides(int nbrAsteroides = 16);
    /**
     * @brief Destructeur de la classe Asteroides
     */
    ~Asteroides();
    /**
     * @brief Méthode permettant d'initialiser les astéroides. Permet de les placer aléatoirement dans la scène
     * @param nb : le nombre d'astéroides de la scène
     */
    void Init(int nb);
    /**
     * @brief Méthode pour dessiner les astéroides dans la scène 3D
     * @param textures : pointeur vers le tableau de toutes les textures
     */
    void Display(GLuint * textures);
    /**
     * @brief Méthode pour détecter la collision entre les astéroides et un autre objet
     * @param object : l'objet à tester
     * @return 1 s'il y a collison entre un astéroide et l'objet, 0 sinon
     */
    int GetCollision(QVector4D object);

private:
    /// La quadrique pour dessiner les astéroides
    GLUquadric* asteroides_;
    /// Le vecteur contenant la position des astéroides (x, y, z) et le rayon des astéroides (w)
    QVector4D *pos;
    /// Le nombre d'astéroides de la scène
    int nbrAsteroides_;
};

#endif // ASTEROIDE_H

#ifndef BACKGROUND_H
#define BACKGROUND_H

#include <GL/glu.h>

/**
 * @author Yoann MASSET
 * @brief La classe Background :
 * Classe qui gère le fond de la scène
 */
class Background
{
public:
    /**
     * @brief Contructeur de la classe Background
     */
    Background();
    /**
     * @brief Destructeur de la classe Background
     */
    ~Background();
    /**
     * @brief Méthode pour dessiner les fond dans la scène 3D
     * @param textures : pointeur vers le tableau de toutes les textures
     */
    void display(GLuint * textures) const;

private:
    /// La quadrique pour dessiner le fond
    GLUquadric * background_ {nullptr};
};

#endif // BACKGROUND_H

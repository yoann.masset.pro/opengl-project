#include "gamewidget.h"

#include <QApplication>
#include <QDebug>
#include <cmath>
#include <algorithm>
#include <QImage>

// Declarations des constantes
const unsigned int WIN = 1000;

GameWidget::GameWidget(QWidget *parent) : QOpenGLWidget{parent}
{
    // Reglage de la taille du widget OpenGL
    setFixedSize(WIN, WIN*0.5);

    // Connexion du timer pour l'animation
    // Sert aussi pour actualiser le contexte OpenGL toutes les 100ms
    connect(&m_AnimationTimer,  &QTimer::timeout, [&] {
        m_TimeElapsed += 1.0f;
        update();
    });
    m_AnimationTimer.setInterval(100);
    m_AnimationTimer.start();
}

GameWidget::~GameWidget()
{

}

void GameWidget::initializeGL()
{
    // Reglage de la couleur de fond
    glClearColor(0.5f, 0.5f, 0.5f, 1.f);

    // Activation du zbuffer
    glEnable(GL_DEPTH_TEST);

    // Activation de la lumière (lumière générale de la scène et lumière pour la station)
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);

    // Initialisation des textures utiles pour le jeu
    glEnable(GL_TEXTURE_2D);
    textures = new GLuint[7];
    glGenTextures(7, textures);

    //Texture 0 : fond
    QImage texture_fond(":/fond.jpg");
    texture_fond = texture_fond.convertToFormat(QImage::Format_RGBA8888);
    glBindTexture(GL_TEXTURE_2D, textures[0]);
    glTexImage2D(GL_TEXTURE_2D, 0, 4, texture_fond.width(), texture_fond.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_fond.bits());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    //Texture 1 : asteroides
    QImage texture_asteroides(":/asteroide.jpg");
    texture_asteroides = texture_asteroides.convertToFormat(QImage::Format_RGBA8888);
    glBindTexture(GL_TEXTURE_2D, textures[1]);
    glTexImage2D(GL_TEXTURE_2D, 0, 4, texture_asteroides.width(), texture_asteroides.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_asteroides.bits());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    //Texture 2 : vaisseau coté
    QImage texture_vaisseau_cote(":/vaisseau_cote.png");
    texture_vaisseau_cote = texture_vaisseau_cote.convertToFormat(QImage::Format_RGBA8888);
    glBindTexture(GL_TEXTURE_2D, textures[2]);
    glTexImage2D(GL_TEXTURE_2D, 0, 4, texture_vaisseau_cote.width(), texture_vaisseau_cote.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_vaisseau_cote.bits());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    //Texture 3 : vaisseau face
    QImage texture_vaisseau_face(":/vaisseau_face.png");
    texture_vaisseau_face = texture_vaisseau_face.convertToFormat(QImage::Format_RGBA8888);
    glBindTexture(GL_TEXTURE_2D, textures[3]);
    glTexImage2D(GL_TEXTURE_2D, 0, 4, texture_vaisseau_face.width(), texture_vaisseau_face.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_vaisseau_face.bits());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    //Texture 4 : vaisseau milieu
    QImage texture_vaisseau_milieu(":/vaisseau_milieu.png");
    texture_vaisseau_milieu = texture_vaisseau_milieu.convertToFormat(QImage::Format_RGBA8888);
    glBindTexture(GL_TEXTURE_2D, textures[4]);
    glTexImage2D(GL_TEXTURE_2D, 0, 4, texture_vaisseau_milieu.width(), texture_vaisseau_milieu.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_vaisseau_milieu.bits());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    //Texture 5 : flag
    QImage texture_flag(":/logo_TSE.png");
    texture_flag = texture_flag.convertToFormat(QImage::Format_RGBA8888);
    glBindTexture(GL_TEXTURE_2D, textures[5]);
    glTexImage2D(GL_TEXTURE_2D, 0, 4, texture_flag.width(), texture_flag.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_flag.bits());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    //Texture 6 : station spatiale
    QImage texture_station(":/station.jpg");
    texture_station = texture_station.convertToFormat(QImage::Format_RGBA8888);
    glBindTexture(GL_TEXTURE_2D, textures[6]);
    glTexImage2D(GL_TEXTURE_2D, 0, 4, texture_station.width(), texture_station.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_station.bits());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Initialisation de la position des asteroides et de la station
    asteroides_.Init(dificulty_);
    station_.Init(asteroides_);
}

void GameWidget::resizeGL(int width, int height)
{
    // Definition du viewport (zone d'affichage)
    glViewport(0, 0, width, height);

    // Definition de la matrice de projection
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    // Définition de la perspective
    gluPerspective(70.f, width/height, 1, 2000);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void GameWidget::paintGL()
{
    // Reinitialisation des tampons
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Lumière directionnelle
    GLfloat light_tab[] = {0.0, 1.0, 0.0, 0.0};
    glLightfv(GL_LIGHT0, GL_POSITION, light_tab);

    GLfloat light_ambiante_tab[] = {1.0, 1.0, 1.0, 1.0};
    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambiante_tab);

    GLfloat light_diffuse_tab[] = {1.0, 1.0, 1.0, 1.0};
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse_tab);

    GLfloat light_speculaire_tab[] = {1.0, 1.0, 1.0, 1.0};
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_speculaire_tab);

    // Conversion des angles en radians
    GLfloat rotationRad = qDegreesToRadians(rotation_);
    GLfloat inclinaisonRad = qDegreesToRadians(inclinaison_);

    // Actualisation de la position du vaisseau en fonction des mouvmements
    QVector3D direction = QVector3D(-sin(rotationRad)*cos(inclinaisonRad), sin(inclinaisonRad), -cos(rotationRad)*cos(inclinaisonRad));
    vaisseau_.setPos(vaisseau_.getPos() += direction * speed_);
    speed_ = 0;

    // Actualisation de la position de la camera
    inclinaisonRad -= qDegreesToRadians(10.f);
    direction = QVector3D(-sin(rotationRad)*cos(inclinaisonRad), sin(inclinaisonRad), -cos(rotationRad)*cos(inclinaisonRad));
    QVector3D cameraPos = vaisseau_.getPos() - direction * 3.f;

    // Mise à jour de la verticale de la camera
    float verticale = 1.f;
    if(inclinaison_ >= 100.f || inclinaison_ < -80.f){
        verticale = -1.f;
        if(inclinaison_ >= 280.f || inclinaison_ <= -260.f){
            verticale = 1.f;
        }
        if(inclinaison_ >= 360.f || inclinaison_ <= -360.f){
            inclinaison_ = 0.f;
        }
    }

    //Coord 1 : droite / gauche
    //Coord 2 : haut / bas
    //Coord 3 : avant / arrière
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    // Definition de la position de la camera
    gluLookAt(cameraPos.x(), cameraPos.y(), cameraPos.z(), vaisseau_.getPos().x(), vaisseau_.getPos().y(), vaisseau_.getPos().z(), 0.f, verticale, 0.f);

    // Affichage du fond et des asteroides
    back_.display(textures);
    asteroides_.Display(textures);

    // Affichage du vaisseau
    glPushMatrix();
    glTranslatef(vaisseau_.getPos().x(), vaisseau_.getPos().y(), vaisseau_.getPos().z());
    glRotatef(rotation_, 0.f, 1.f, 0.f);
    glRotatef(inclinaison_, 1.f, 0.f, 0.f);
    vaisseau_.display(textures);
    glPopMatrix();

    // Affichage de la station
    station_.Display(textures, m_TimeElapsed);

    // Vérifie la fin de partie
    if(!pause_){
        checkEnd();
    }
}

void GameWidget::checkEnd(){
    QVector4D ship = vaisseau_.getPos().toVector4D();
    ship.setW(vaisseau_.getLargeur());
    if(asteroides_.GetCollision(ship) == 1){
        // Collison entre le vaisseau et un des asteroides
        emit lose();
    } else if(station_.GetCollision(ship) == 1){
        // Collison entre le vaisseau et la station
        emit win();
    }
}

void GameWidget::newGame(){
    // Réinitialisation des paramètres pour une nouvelle partie
    vaisseau_.setPos(QVector3D(0.f, 0.f, 0.f));
    speed_ = 0.f;
    rotation_ = 0.f;
    inclinaison_ = 0.f;

    // Réinitialisation des asteroides et de la station
    asteroides_.Init(dificulty_);
    station_.Init(asteroides_);
}

void GameWidget::widgetPause() {
    // Lors de la pause, arret du timer pour l'animation et le rafraichissement d'OpenGL
    pause_ = !pause_;
    if(pause_) {
        m_AnimationTimer.stop();
    } else {
        m_AnimationTimer.start();
    }
}

void GameWidget::setEasy(){
    dificulty_ = EASY;
    newGame();
}

void GameWidget::setNormal(){
    dificulty_ = NORMAL;
    newGame();
}

void GameWidget::setHard(){
    dificulty_ = HARD;
    newGame();
}

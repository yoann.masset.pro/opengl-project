#ifndef GAMEWIDGET_H
#define GAMEWIDGET_H

#include <QOpenGLWidget>
#include <QDebug>
#include <QTimer>

#include "background.h"
#include "vaisseau.h"
#include "asteroides.h"
#include "station.h"

/**
 * @brief La vitesse du vaisseau à chaque déplacement
 */
const float SPEED = 2.f;
/**
 * @brief L'angle de rotation à chaque rotation
 */
const float ANGLE = 5.f;
/**
 * @brief Le type énuméré pour la dificulté :
 * Correspond au nombre d'astéroides (facile : 16, moyen : 25, difficile : 40)
 */
enum Dificulty{
    EASY = 16,
    NORMAL = 25,
    HARD = 40,
};

/**
 * @authors Diane PERES & Yoann MASSET
 * @brief La classe GameWidget :
 * La classe qui permet de gérer le widget OpenGL
 */
class GameWidget : public QOpenGLWidget
{
    Q_OBJECT
public:
    /**
     * @brief Constructeur de la classe GameWidget
     * @param parent : le widget parent
     */
    explicit GameWidget(QWidget *parent = nullptr);
    /**
     * @brief Destructeur de la classe GameWidget
     */
    ~GameWidget();
    /**
     * @brief Méthode pour initialiser le contexte OpenGL
     */
    void initializeGL();
    /**
     * @brief Méthode pour redimensionner le contexte OpenGL
     * @param width : la largeur du widget
     * @param height : la hauteur du widget
     */
    void resizeGL(int width, int height);
    /**
     * @brief Méthode pour dessiner les éléments OpenGL
     */
    void paintGL();
    /**
     * @brief Méthode pour vérifier s'il y a eu collisions et donc une fin de partie
     */
    void checkEnd();

signals:
    /// Signal emit lors d'une partie gagnée
    void win();
    /// Signal emit lors d'une partie perdue
    void lose();

private:
    /// Attribut pour stocker le temps écoulé
    float m_TimeElapsed { 0.0f };
    /// Le timer pour animer la scène
    QTimer m_AnimationTimer;
    /// L'objet Vaisseau relié au vaisseau de la scène
    Vaisseau vaisseau_;
    /// L'objet Asteroides relié aux astéroides de la scène
    Asteroides asteroides_;
    /// L'objet Station relié à la station spaciale de la scène
    Station station_;
    /// L'objet Background relié au Background de la scène
    Background back_;
    /// Pointeur vers le tableau de toutes les textures
    GLuint * textures;
    /// La vitesse du vaisseau
    GLfloat speed_ = 0.f;
    /// L'angle de rotation du vaisseau dans la scène
    GLfloat rotation_ = 0.f;
    /// L'angle d'inclinaison du vaisseau dans la scène
    GLfloat inclinaison_ = 0.f;
    /// La dificulté de la partie
    Dificulty dificulty_ = EASY;
    /// L'Attribut de pause
    bool pause_ = 0;

public slots:
    /// Le slot executé lorsque le vaisseau tourne à droite
    void goRight() { rotation_ -= ANGLE;};
    /// Le slot executé lorsque le vaisseau tourne à gauche
    void goLeft() { rotation_ += ANGLE;};
    /// Le slot executé lorsque le vaisseau avance
    void goForward() { speed_ += SPEED;};
    /// Le slot executé lorsque le vaisseau monte
    void goUp() { inclinaison_ += ANGLE;};
    /// Le slot executé lorsque le vaisseau descend
    void goDown() { inclinaison_ -= ANGLE;};
    /// Le slot executé lorsqu'une nouvelle partie commence
    void newGame();
    /// Le slot executé lorsqu'il y a pause
    void widgetPause();
    /// Le slot executé lorsque la dificulté est mise à facile
    void setEasy();
    /// Le slot executé lorsque la dificulté est mise à moyen
    void setNormal();
    /// Le slot executé lorsque la dificulté est mise à difficile
    void setHard();
};

#endif // GAMEWIDGET_H
